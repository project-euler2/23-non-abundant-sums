def find_all_abundant_numbers(number_limit):
    abundant_numbers = []
    for i in range(1,number_limit):
        divisors = []
        for j in range(1,int(i/2 + 1)):
            if i % j == 0:
                divisors.append(j)
        if sum(divisors) > i :
            abundant_numbers.append(i)
    return abundant_numbers

def find_sum_by2_of_abundant_number(number_limit):
    abundant_numbers = find_all_abundant_numbers(number_limit)
    sum_2_abundant_number = []
    for abund_num in abundant_numbers:
        for num_abund in abundant_numbers:
            if (abund_num + num_abund) < number_limit+1:
                sum_2_abundant_number.append(abund_num + num_abund)
    only_not_sum2_abundant = set_approach(list(range(1,number_limit+1)), list(dict.fromkeys(sum_2_abundant_number)))
    return only_not_sum2_abundant

def set_approach(a,b):
    return list(set(a)-set(b))

print(sum(find_sum_by2_of_abundant_number(28123)))